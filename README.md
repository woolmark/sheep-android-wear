Sheep for Android Wear
----------------------------------------------------------------------------
Sheep for Android Wear is an implementation [sheep] for [android-wear].

Get it on Google Play
----------------------------------------------------------------------------
[![Get it on Google Play][get_it_logo]](https://play.google.com/store/apps/details?id=org.bitbucket.woolmark.sheep.android.wear)

How to build
-----------------------------------------------------------------------------

Run ant

    % ant debug

or, if you want to build on Eclipse,

 1. Build sheep-android-wear project
 2. Copy `bin/sheep-android-wear.apk` to `subprojects/package-app/res/raw/wearable_app.apk`
 3. Build sheep-android-wear-pkg project

License
-----------------------------------------------------------------------------
[WTFPL] 

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[android-wear]: http://www.android.com/wear/ "Android Wear"
[get_it_logo]: https://developer.android.com/images/brand/en_generic_rgb_wo_45.png
[get_it]: https://play.google.com/store/apps/details?id=org.bitbucket.woolmark.sheep.android.wear
[WTFPL]: http://www.wtfpl.net "WTFPL"


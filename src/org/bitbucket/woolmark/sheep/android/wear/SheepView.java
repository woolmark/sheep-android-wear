package org.bitbucket.woolmark.sheep.android.wear;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;


/**
 * android sheep canvas.
 *
 * @author takimura
 */
public class SheepView extends ImageView implements Runnable {

    private static final int FPS = 10;

    private static final int DEFAULT_WIDTH = 120;

    private static final int DEFAULT_HEIGHT = 120;

    private static final float GROUND_HEIGHT_RATIO = 0.9f;

    private static final int X_MOVING_DIST = 5;

    private static final int Y_MOVING_DIST = 3;

    private static final int JUMP_FRAME = 4;

    private static final int GROUND_COLOR = Color.rgb(150, 255, 150);

    private static final int SKY_COLOR = Color.rgb(150, 150, 255);

    private int mScale = 1;

    private int mGroundHeight;

    private int mStrechLeg = 0;

    private int mSheepCount;

    private int[][] mSheepFlock = new int[100][4];

    private boolean mAddSheep = false;

    private Bitmap[] mSheepImage;

    private Bitmap mFenceImage;

    private Random mRand;

    private OnSheepCountUpListener mOnSheepCountUpListener;

    public SheepView(final Context context) {
        this(context, null);
    }

    public SheepView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SheepView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        /* create object */
        mRand = new Random();

        // load the sheep images
        mSheepImage = new Bitmap[2];
        mSheepImage[0] = BitmapFactory.decodeResource(getResources(), R.drawable.sheep00);
        mSheepImage[1] = BitmapFactory.decodeResource(getResources(), R.drawable.sheep01);

        // load a fence image
        mFenceImage = BitmapFactory.decodeResource(getResources(), R.drawable.fence);

        // set the sheep position
        for (int i = 0; i < mSheepFlock.length; i++) {
            removeSheep(i);
        }

        addSheep(0);

        (new Handler()).post(this);

    }

    @Override
    public void draw(final Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(false);
        paint.setFilterBitmap(false);

        paint.setColor(SKY_COLOR);
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);

        paint.setColor(GROUND_COLOR);
        canvas.drawRect(0, getHeight() - mGroundHeight, getWidth(), getHeight(), paint);

        // draw fence
        Matrix matrix = new Matrix();
        matrix.setScale(mScale, mScale);
        matrix.postTranslate(
                (getWidth() - mFenceImage.getWidth() * mScale) / 2,
                getHeight() - mFenceImage.getHeight() * mScale);
        canvas.drawBitmap(mFenceImage, matrix, paint);

        // draw sheep
        for (int i = 0; i < mSheepFlock.length; i++) {
            int[] sheep = mSheepFlock[i];

            if (sheep[1] < 0) {
                continue;
            }

            matrix = new Matrix();
            matrix.setScale(mScale, mScale);
            matrix.postTranslate(sheep[0], sheep[1]);
            if (sheep[2] >= 0) {
                canvas.drawBitmap(mSheepImage[1], matrix, paint);
            } else {
                canvas.drawBitmap(mSheepImage[mStrechLeg], matrix, paint);
            }
        }

    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            mAddSheep = true;
            return true;
        case MotionEvent.ACTION_UP:
            mAddSheep = false;
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void run() {
        calc();
        invalidate();

        (new Handler(getContext().getMainLooper())).postDelayed(this, 1000 / FPS);
    }

    public int getSheepCount() {
        return mSheepCount;
    }

    public void setSheepCount(final int count) {
        mSheepCount = count;
    }

    public void setOnSheepCountUpListener(final OnSheepCountUpListener listener) {
        mOnSheepCountUpListener = listener;
    }

    @Override
    protected void onSizeChanged(final int w, final int h , final int oldw, final int oldh) {

        if (w < h) {
            mScale = (int) (w / DEFAULT_WIDTH);
        } else {
            mScale = (int) (h / DEFAULT_HEIGHT);
        }

        mGroundHeight = (int) (mFenceImage.getHeight() * mScale * GROUND_HEIGHT_RATIO);

        for (int i = 0; i < mSheepFlock.length; i++) {
            removeSheep(i);
        }
        addSheep(0);

    }

    /**
     * calculate.
     */
    private void calc() {

        // add a sheep per a frame
        if (mAddSheep) {
            for (int i = 0; i < mSheepFlock.length; i++) {
                if (mSheepFlock[i][1] < 0) {
                    addSheep(i);
                    break;
                }
            }
        }

        // run the sheep
        for (int i = 0; i < mSheepFlock.length; i++) {
            int[] sheep = mSheepFlock[i];

            if (sheep[1] < 0) {
                continue;
            }

            // run
            sheep[0] -= mScale * X_MOVING_DIST;

            if (sheep[2] < 0 && sheep[3] <= sheep[0]
                    && sheep[0] < sheep[3] + mScale * X_MOVING_DIST)  {
                sheep[2] = 0;
            }

            // jump
            if (sheep[2] >= 0) {
                if (sheep[2] < JUMP_FRAME) {
                    sheep[1] -= mScale * Y_MOVING_DIST;
                } else if (sheep[2] < JUMP_FRAME * 2) {
                    sheep[1] += mScale * Y_MOVING_DIST;
                } else {
                    sheep[2] = Integer.MIN_VALUE;
                }

                sheep[2] += 1;
            }

            if (sheep[2] == JUMP_FRAME) {
                mSheepCount += 1;
                if (mOnSheepCountUpListener != null) {
                    mOnSheepCountUpListener.onSheepCountUp(mSheepCount);
                }
            }

            // remove a frame outed sheep
            if (sheep[0] < -1 * mScale * Y_MOVING_DIST) {
                if (i == 0) {
                    addSheep(i);
                } else {
                    removeSheep(i);
                }
            }

        }

        mStrechLeg = 1 - mStrechLeg;

    }

    private void addSheep(final int index) {
        if (getHeight() > 0) {
            mSheepFlock[index][0] = getWidth() + mSheepImage[0].getWidth() * mScale;
            mSheepFlock[index][1] = getHeight() - mGroundHeight + mRand.nextInt(
                    mGroundHeight - mSheepImage[0].getHeight() * mScale);
            mSheepFlock[index][2] = Integer.MIN_VALUE;
            mSheepFlock[index][3] = calcJumpX(mSheepFlock[index][1]);
        }
    }

    private void removeSheep(final int index) {

        mSheepFlock[index][0] = 0;
        mSheepFlock[index][1] = -1;
        mSheepFlock[index][2] = 0;
        mSheepFlock[index][3] = 0;

    }

    private int calcJumpX(final int y) {
        // y = -1 * fence.height / fence.width * (x - (width - fence.width) / 2) + height

        int w = getWidth();
        int h = getHeight();
        int fw = mFenceImage.getWidth() * mScale;
        int fh = mFenceImage.getHeight() * mScale;

        return -1 * (y - h) * fw / fh + (w - fw) / 2;

    }

    public interface OnSheepCountUpListener {

        void onSheepCountUp(final int number);

    }

}

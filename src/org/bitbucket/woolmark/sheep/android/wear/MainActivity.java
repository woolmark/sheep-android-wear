package org.bitbucket.woolmark.sheep.android.wear;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.widget.TextView;

public class MainActivity extends Activity implements SheepView.OnSheepCountUpListener {

    private static final String PREF_SHEEP = "sheep";

    private static final String KEY_SHEEP_NUMBER = "sheep_number";

    @Override
    public void onSheepCountUp(final int number) {
        TextView counter = (TextView) findViewById(R.id.sheep_counter);
        counter.setText(getString(R.string.sheep_count, number));
    }

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.main_activity);

        ((WatchViewStub) findViewById(R.id.stub)).setOnLayoutInflatedListener(
                new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(final WatchViewStub view) {
                SheepView sheepView = (SheepView) findViewById(R.id.sheep_view);
                if (sheepView != null) {
                    sheepView.setOnSheepCountUpListener(MainActivity.this);

                    int count = loadSheepCount(MainActivity.this);
                    sheepView.setSheepCount(count);
                    onSheepCountUp(count);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        SheepView sheepView = (SheepView) findViewById(R.id.sheep_view);
        if (sheepView != null) {
            saveSheepCount(this, sheepView.getSheepCount());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private int loadSheepCount(final Context context) {

        int count = 0;

        SharedPreferences prefs = context.getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
        if (prefs != null) {
            count = prefs.getInt(KEY_SHEEP_NUMBER, 0);
        }

        return count;
    }

    private void saveSheepCount(final Context context, final int count) {

        SharedPreferences prefs = context.getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
        if (prefs != null) {
            prefs.edit().putInt(KEY_SHEEP_NUMBER, count).commit();
        }

    }

}
